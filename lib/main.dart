// Copyright 2017 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:local_notifications/local_notifications.dart';

final Map<String, Item> _items = <String, Item>{};

Item _itemForMessage(Map<String, dynamic> message) {
  final String itemId = message['id'];
  final Item item = _items.putIfAbsent(itemId, () => new Item(itemId: itemId))
    ..status = message['status'];
  return item;
}

class Item {
  Item({this.itemId});

  final String itemId;

  StreamController<Item> _controller = new StreamController<Item>.broadcast();

  Stream<Item> get onChanged => _controller.stream;

  String _status;

  String get status => _status;

  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<Null>> routes = <String, Route<Null>>{};

  Route<Null> get route {
    final String routeName = '/detail/$itemId';
    return routes.putIfAbsent(
      routeName,
      () => new MaterialPageRoute<Null>(
            settings: new RouteSettings(name: routeName),
            builder: (BuildContext context) => new DetailPage(itemId),
          ),
    );
  }
}

class DetailPage extends StatefulWidget {
  DetailPage(this.itemId);

  final String itemId;

  @override
  _DetailPageState createState() => new _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Item _item;
  StreamSubscription<Item> _subscription;

  @override
  void initState() {
    super.initState();
    _item = _items[widget.itemId];
    _subscription = _item.onChanged.listen((Item item) {
      if (!mounted) {
        _subscription.cancel();
      } else {
        setState(() {
          _item = item;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Item ${_item.itemId}"),
      ),
      body: new Material(
        child: new Center(child: new Text("Item status: ${_item.status}")),
      ),
    );
  }
}

class PushMessagingExample extends StatefulWidget {
  @override
  _PushMessagingExampleState createState() => new _PushMessagingExampleState();
}

class _PushMessagingExampleState extends State<PushMessagingExample> {
  String _homeScreenText = "Waiting for token...";
  bool _topicButtonsDisabled = false;

  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  final TextEditingController _topicController =
      new TextEditingController(text: 'topic');

  Widget _buildDialog(BuildContext context, Item item) {
    return new AlertDialog(
      content: new Text("Item ${item.itemId} has been updated"),
      actions: <Widget>[
        new FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        new FlatButton(
          child: const Text('SHOW'),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, _itemForMessage(message)),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  void _showNotification() async {
    await LocalNotifications.createNotification(
        title: "Basic", content: "Notification", id: 0);
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print("onMessage: ${message.toString()}");
        //_showItemDialog(message);
        LocalNotifications.createNotification(
            id: 0,
            title: message['title'],
            content: message['content'],
            androidSettings: new AndroidSettings(
              isOngoing: false,
              channel: channel,
              priority: AndroidNotificationPriority.HIGH,
            ),
            onNotificationClick: new NotificationAction(
                actionText: "some action",
                callback: removeNotify,
                payload: ""));
      },
      onLaunch: (Map<String, dynamic> message) {
        print("onLaunch: $message");
        _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) {
        print("onResume: $message");
        _navigateToItemDetail(message);
      },
    );

    _firebaseMessaging.subscribeToTopic("Lukas_tester");
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        _homeScreenText = "$token";
      });
      print(_homeScreenText);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: const Text('Push Messaging Demo'),
        ),
        // For testing -- simulate a message being received
        floatingActionButton: new FloatingActionButton(
          onPressed: () => _showItemDialog(<String, dynamic>{
                "id": "2",
                "status": "out of stock",
              }),
          tooltip: 'Simulate Message',
          child: const Icon(Icons.message),
        ),
        body: new Material(
          child: new Column(
            children: <Widget>[
              new Center(
                child: new Column(
                  children: <Widget>[
                    _getBasicNotification(),
                    _getNotificationWithImage(),
                    _getUndismissableNotification(),
                    _getRemoveNotification(),
                    _getNotificationWithCallbackAndPayload(),
                    _getNotificationWithCallbackAndPayloadInBackground(),
                    _getNotificationWithMultipleActionsAndPayloads(),
                    _getNotificationWithAnonymousFunctionAsCallback(),
                    _getAddNotificationChannelButton(),
                    _getRemoveNotificationChannelButton(),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  void _clearTopicText() {
    setState(() {
      _topicController.text = "";
      _topicButtonsDisabled = true;
    });
  }

  String _imageUrl = 'https://flutter.io/images/catalog-widget-placeholder.png';
  String _text;
  bool loggingEnabled = false;

  onNotificationClick(String payload) {
    setState(() {
      _text = 'in onNotificationClick with payload: $payload';
      print(_text);
    });
  }

  onReplyClick(String payload) {
    setState(() {
      _text = 'in onReplyClick with payload: $payload';
      print(_text);
    });
    LocalNotifications.removeNotification(0);
  }

  void removeNotify(String payload) async {
    await LocalNotifications.removeNotification(0);
  }

  static const AndroidNotificationChannel channel =
      const AndroidNotificationChannel(
    id: 'default_notification11',
    name: 'CustomNotificationChannel',
    description: 'Grant this app the ability to show notifications',
    importance: AndroidNotificationChannelImportance.HIGH,
    vibratePattern: AndroidVibratePatterns.DEFAULT,
  );

  Widget _getAddNotificationChannelButton() {
    return new RaisedButton(
      child: new Text('Create a notification channel (Android 8.0+)'),
      onPressed: () async {
        await LocalNotifications.createAndroidNotificationChannel(
            channel: channel);
      },
    );
  }

  Widget _getRemoveNotificationChannelButton() {
    return new RaisedButton(
      child: new Text('Remove a notification channel (Android 8.0+)'),
      onPressed: () async {
        await LocalNotifications.removeAndroidNotificationChannel(
            channel: channel);
      },
    );
  }

  Widget _enableLogging() {
    return new Row(
      children: <Widget>[
        new Switch(
            value: loggingEnabled,
            onChanged: (val) async {
              setState(() {
                loggingEnabled = val;
              });
              await LocalNotifications.setLogging(val);
            }),
        new Text('Enable or Disable logging')
      ],
    );
  }

  Widget _getBasicNotification() {
    return new RaisedButton(
      onPressed: () async {
        await LocalNotifications.createNotification(
            id: 0,
            title: 'Basic',
            content: 'some basic notification',
            androidSettings: new AndroidSettings(
              isOngoing: false,
              channel: channel,
              priority: AndroidNotificationPriority.HIGH,
            ),
            onNotificationClick: new NotificationAction(
                actionText: "some action",
                callback: removeNotify,
                payload: ""));
      },
      child: new Text('Create basic notification'),
    );
  }

  Widget _getNotificationWithImage() {
    return new RaisedButton(
        onPressed: () async {
          await LocalNotifications.createNotification(
            id: 0,
            title: 'Image',
            content: 'some notification with an image',
            imageUrl: _imageUrl,
          );
        },
        child: new Text('Create notification with image'));
  }

  Widget _getUndismissableNotification() {
    return new RaisedButton(
        onPressed: () async {
          await LocalNotifications.createNotification(
              id: 0,
              title: 'No swiping',
              content: 'Can\'t swipe this away',
              imageUrl: _imageUrl,
              androidSettings: new AndroidSettings(isOngoing: true));
        },
        child: new Text('Create undismissable notification'));
  }

  Widget _getRemoveNotification() {
    return new RaisedButton(
      onPressed: () async {
        // remove notificatino by id,
        // all examples don't provide an id, so it defaults to 0
        await LocalNotifications.removeNotification(0);
      },
      child: new Text('Remove notification'),
    );
  }

  Widget _getNotificationWithCallbackAndPayload() {
    return new RaisedButton(
      onPressed: () async {
        await LocalNotifications.createNotification(
          id: 0,
          title: 'Callback and payload notif',
          content: 'Some content',
          onNotificationClick: new NotificationAction(
              actionText: "some action",
              // Note: action text gets ignored here, as android can't display this anywhere
              callback: onNotificationClick,
              payload: "some payload"),
        );
      },
      child: new Text('Create notification with payload and callback'),
    );
  }

  Widget _getNotificationWithCallbackAndPayloadInBackground() {
    return new RaisedButton(
      onPressed: () async {
        await LocalNotifications.createNotification(
          id: 0,
          title: 'Callback and payload notif',
          content: 'Some content',
          onNotificationClick: new NotificationAction(
              actionText: "some action",
              // Note: action text gets ignored here, as android can't display this anywhere
              callback: onNotificationClick,
              payload: "some payload without launching the app",
              launchesApp: false),
        );
      },
      child: new Text(
          'Create notification that executes callback without launching app'),
    );
  }

  Widget _getNotificationWithMultipleActionsAndPayloads() {
    return new RaisedButton(
      onPressed: () async {
        await LocalNotifications.createNotification(
            id: 0,
            title: 'Multiple actions',
            content: '... and unique callbacks and/or payloads for each',
            imageUrl: _imageUrl,
            onNotificationClick: new NotificationAction(
                actionText: "some action",
                callback: onNotificationClick,
                payload: "some payload",
                launchesApp: false),
            actions: [
              new NotificationAction(
                  actionText: "First",
                  callback: onReplyClick,
                  payload: "firstAction",
                  launchesApp: true),
              new NotificationAction(
                  actionText: "Second",
                  callback: onReplyClick,
                  payload: "secondAction",
                  launchesApp: false),
              new NotificationAction(
                  actionText: "Third",
                  callback: onReplyClick,
                  payload: "thirdAction",
                  launchesApp: false),
            ]);
      },
      child: new Text('Create notification with multiple actions'),
    );
  }

  Widget _getNotificationWithAnonymousFunctionAsCallback() {
    return new RaisedButton(
        child: new Text(
            'Create notification with anonymous function as callback using a callbackName'),
        onPressed: () async {
          await LocalNotifications.createNotification(
              id: 0,
              title: 'Anonymous callback',
              content:
                  '... using anonymous callback with provided callbackName',
              onNotificationClick: new NotificationAction(
                  actionText: '', //ignored
                  callback: (String payload) {
                    setState(() {
                      _text = payload;
                      print(_text);
                    });
                  },
                  payload: 'payload with anonymous function',
                  callbackName: 'anonymousName'),
              actions: [
                new NotificationAction(
                  actionText: 'anon',
                  callback: (String payload) {
                    setState(() {
                      _text = payload;
                      print(_text);
                    });
                  },
                  payload: 'payload from action with anonymous action',
                  callbackName: 'anonymousAction',
                )
              ]);
        });
  }
}

void main() {
  runApp(
    new MaterialApp(
      home: new PushMessagingExample(),
    ),
  );
}
